# OBS! Not live anymore!



# I-VIP
### A privileged online platform for sharing content.

The website is online at [http://i-vip.online](http://i-vip.online) in all it's unfinshed glory. [PHP](https://www.addedbytes.com/blog/if-php-were-british/).

***

To get it running localhost some requirements:
* PHP 5.6
* MySQL 5.5.46

Default MySQL users and credentials:
* username: `i-vip`
* pass: `OgprGYcJLg`

### Server info
Input your username and then the ip. You will be asked for a password you have provided yourself.

`ssh tanya@46.101.179.203`


***
## "I have made a change" guide

#### 1. On your own machine in your working folder
* `git pull`
* `git add --all`
* `git commit -m 'message'`
* `git push origin master`

#### 2. On the server in the `/var/www/i_vip`
* `sudo git pull`

***

Create a user at `http://i-vip.online/register.php` and thereafter try to login at `http://i-vip.online/`.

To create the database an `init.sql` file has been supplied in the repository.

***

## Routes (worth noting atm)

### [http://i-vip.online/register.php](http://i-vip.online/register.php)
### [http://i-vip.online/](http://i-vip.online/)