CREATE DATABASE IF NOT EXISTS i_vip COLLATE utf8_general_ci;

CREATE USER 'i-vip'@'localhost' IDENTIFIED BY 'OgprGYcJLg';

GRANT SELECT, INSERT, UPDATE, DELETE ON i_vip.* TO 'i-vip'@'localhost';

CREATE TABLE `roles` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(24) NOT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `sessions` (
  `sid` varchar(40) NOT NULL,
  `expiry` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`sid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `two_way_methods` (
  `two_way_method_id` int(11) NOT NULL AUTO_INCREMENT,
  `method` varchar(250) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`two_way_method_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `users` (
  `user_key` char(8) NOT NULL,
  `username` varchar(30) NOT NULL,
  `user_alias` varchar(30) NOT NULL,
  `pwd` varchar(255) NOT NULL,
  `activated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `login_key` char(8) NOT NULL,
  `login_key_expiry` int(10) unsigned NOT NULL,
  `two_way_methods_id` int(2) unsigned NOT NULL DEFAULT '4',
  PRIMARY KEY (`user_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `user_roles` (
  `roles_role_id` int(11) NOT NULL,
  `users_user_key` char(8) NOT NULL,
  KEY `roles_role_id` (`roles_role_id`),
  KEY `users_user_key` (`users_user_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `wall_chat` (
  `w_id` int(11) NOT NULL AUTO_INCREMENT,
  `users_user_key` char(8) NOT NULL,
  `message` text NOT NULL,
  `creation_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`w_id`),
  KEY `users_user_key` (`users_user_key`),
  CONSTRAINT `wall_chat_ibfk_4` FOREIGN KEY (`users_user_key`) REFERENCES `users` (`user_key`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
