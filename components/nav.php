<nav class="teal darken-1" role="navigation" style="margin-bottom:10px;">
  <div class="nav-wrapper container"><a id="logo-container" href="/wall.php" class="brand-logo">i-VIP</a>
    <ul class="right">
      <li style="margin-right:15px;" class="hide-on-med-and-down">
        Logged in as <?= htmlentities($_SESSION['user_alias']); ?>
      </li>
      <li>
        <a style="font-weight:bold;" href="/dashboard.php">Dashboard</a>
      </li>
      <li class="hide-on-med-and-down"><?php include 'logout_button.php'; ?></li>
    </ul>
  </div>
</nav>
<script src="js/init.js" charset="utf-8"></script>
