<?php

include '../components/profile_pic.php';

if(isset($_FILES['fileToUpload'])){
    //create array for holding error messages
    $errors = array();
    $file_name = $_SESSION['user_key'] . ".png";
    $file_size = $_FILES['fileToUpload']['size'];
    $file_tmp = $_FILES['fileToUpload']['tmp_name'];
    $file_type = $_FILES['fileToUpload']['type'];
    $file_ext = strtolower(end(explode('.', $_FILES['fileToUpload']['name'])));
    $target_dir = "../uploads/profile_pics/";
    $target_file = $target_dir . basename($file_name);

    //make array with allowed file types to limit what files the users are allowed to upload/share
    $expressions = array("jpeg", "jpg", "png");


    //if the allowed file types is not equals to the ending of the image file name
    if(in_array($file_ext, $expressions) === false){
        $errors[] = "extension is not allowed, please chose a JPEG or PNG file.";
    }

    //check if php is in the filename to avoid filename.php.jpg

    $pos = strpos(strtolower($file_name), "php");
    if($pos !== false){
        $errors[] ="File cannot contain php in the filename";
    }

    //make sure file size does not break the system
    if($file_size > 2097152){
        $errors[] = "File size must be excatly 2 MB in size or below";
    }


    //if there is no errors
    if(empty($errors) == true){
        // upload temp file to the images folder
        $success = move_uploaded_file($file_tmp, "uploads/profile_pics/".$file_name);
        if(!$success){
            echo "<p>Unable to save file.</p>";
            exit;
        }else{
            echo "Your image was uploaded successfully";
        }

        unset($_POST);
    }else{
        foreach ($errors as &$value) {
            echo $value;
        }
    }


}

?>

<h3>Upload an Avatar</h3>

<div class="circle z-depth-1 profile-img" style="background-image: url('<?php echo $profile_pic ?>'); position:initial; margin-left:auto; margin-right:auto;"></div>

<form action="dashboard.php" method="post" enctype="multipart/form-data">
    <div class="file-field input-field">
      <div class="btn">
        <span>File</span>
        <input type="file" name="fileToUpload" id="fileToUpload">
      </div>
      <div class="file-path-wrapper">
        <input class="file-path validate" type="text">
      </div>
    </div>
    <div class="row">
      <div class="input-field col s12 center-align">
        <button type="submit" name="submit" id="submit" value="Upload Image" class="btn-large waves-effect waves-light orange">Upload Image</button>
      </div>
    </div>

  </form>
