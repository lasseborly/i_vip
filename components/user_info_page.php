<?php

require_once '../src/db_connect.php';
$user_key = $_SESSION["user_key"];

$sql = 'SELECT * FROM users WHERE user_key = :user_key';
$stmt = $db->prepare($sql);
$stmt->bindParam(':user_key', $user_key);
$stmt->execute();

$user = $stmt->fetch();

if (!empty($_POST['token'])) {
  if (hash_equals($_POST['token'], $_SESSION['token'])) {

    if (isset($_POST['update']))  {

      $stmtUpdate = $db->prepare('UPDATE users SET users.user_alias=:user_alias, users.username=:username WHERE users.user_key = :user_key');
      $stmtUpdate->bindParam(':user_key', $user_key);
      $stmtUpdate->bindParam(':user_alias', htmlentities($_POST['user_alias']));
      $stmtUpdate->bindParam(':username', htmlentities($_POST['username']));
      $stmtUpdate->execute();

      $_SESSION['username'] = htmlentities($_POST['username']);
      $_SESSION['user_alias'] = htmlentities($_POST['user_alias']);

      header('Location: dashboard.php');
      exit;
    }
    else {
      header('Location: index.php');
      exit;
    }

  } else {

  }
}


include_once "../src/create_token.php";

?>
<form method="post" class="col s12">
  <input type="hidden" name="token" value="<?= $token ?>" />
  <div class="row">
    <div class="input-field col s12 l6">
      <input id="user_alias" type="text" name="user_alias" class="validate" value="<?= htmlentities($user['user_alias']); ?>">
      <label for="user_alias">User Alias</label>
    </div>
    <div class="input-field col s12 l6">
      <input id="email" type="email" name="username" class="validate" value="<?= htmlentities($user['username']); ?>">
      <label for="email">Email</label>
    </div>
  </div>
  <div class="row">
    <div class="input-field col s12 center-align">
      <button type="submit" name="update" id="update" value="Update Account" class="btn-large waves-effect waves-light orange">Submit Changes</button>
    </div>
  </div>
</form>
