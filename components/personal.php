<div class="col m6 s12">
  <div class="written">
    <div class="card small">
      <div class="card-image">
        <nav class="teal darken-1" role="navigation">
          <div class="nav-wrapper">
            <ul class="hide-on-med-and-down">
              <li>
               <a><i class="material-icons">clear</i></a>
              </li>
              <li>
                <a><i class="material-icons">edit</i></a>
              </li>
              <li>
                <a><i class="material-icons">visibility_off</i></a>
              </li>
              <li>
                <a><i class="material-icons">visibility</i></a>
              </li>
              <li>
                <a><i class="material-icons">stars</i></a>
              </li>
            </ul>
            <ul class="right">
              <li>
                <a style="color: #b71c1c; text-shadow: 0 0 3px #ff1744;"><i class=" material-icons">thumb_down</i></a>
              </li>
              <li>
               <a style="color: #1de9b6; text-shadow: 0 0 3px #a7ffeb;"><i class=" material-icons">thumb_up</i></a>
              </li>
            </ul>
          </div>
        </nav>
      </div>
      <div class="card-content">
        <h5>I have been a good boy!</h5>
        <span class="card-title activator grey-text text-darken-4">Click to share your thoughts<i class="material-icons right"></i></span>
      </div>
      <div class="card-reveal">
        <span class="card-title grey-text text-darken-4"><p>I have been a good boy!</p><i class="material-icons right">close</i></span>
        <br>
        <br>
        Write your response:<br><textarea id="textarea1" class="materialize-textarea"  length="120"></textarea>
        </textarea><br>
      </div>
    </div>
  </div>
</div>
