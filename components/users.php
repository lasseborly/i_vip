<?php

require_once '../src/db_connect.php';

$getUsers = $db->prepare('SELECT users.activated, users.username, users.user_alias, users.user_key, roles.role_name FROM users JOIN user_roles ON users.user_key = user_roles.users_user_key JOIN roles ON roles.role_id = user_roles.roles_role_id');
$getUsers->execute();
$stored = $getUsers->fetchAll();

?>

<table class="centered responsive-table">
  <thead>
    <tr>
      <th data-field="id">ID</th>
      <th data-field="name">Alias</th>
      <th data-field="email">Email</th>
      <th data-field="role">Role</th>
      <th data-field="activated">Activated</th>
    </tr>
  </thead>
  <tbody>
    <?php
    foreach ($stored as $user) {

      $activated = '<i class="small material-icons green-text">done</i>';
      if ($user['activated'] != 1) {
        $activated = '<i class="small material-icons red-text">not_interested</i>';
      }


      echo "<tr><td>" .
      htmlentities($user['user_key']) .
      "</td><td>" .
      htmlentities($user['user_alias']) .
      "</td><td>" .
      htmlentities($user['username']) .
      "</td><td>" .
      htmlentities($user['role_name']) .
      "</td><td>" .
      $activated .
      "</td></tr>";
    }
    ?>
  </tbody>
</table>
