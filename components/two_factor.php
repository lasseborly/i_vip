<?php
require_once '../src/init.php';
require_once '../src/db_connect.php';

$sql = 'SELECT * FROM two_way_methods';
$stmt = $db->prepare($sql);
$stmt->execute();
$stored = $stmt->fetchAll();

$sqlUser = 'SELECT users.two_way_methods_id FROM users WHERE users.user_key = :user_key';
$stmtUser = $db->prepare($sqlUser);
$stmtUser->bindParam(':user_key', $_SESSION['user_key']);
$stmtUser->execute();
$userMethod = $stmtUser->fetch();

if (!empty($_POST['token'])) {
    if (hash_equals($_POST['token'], $_SESSION['token'])) {

      if (isset($_POST['group1'])) {

        $stmtLoginKey = $db->prepare('UPDATE users SET users.two_way_methods_id=:two_way_methods_id WHERE users.user_key = :user_key');
        $stmtLoginKey->bindParam(':two_way_methods_id', $_POST['group1']);
        $stmtLoginKey->bindParam(':user_key', $_SESSION['user_key']);
        $stmtLoginKey->execute();

      }

    } else {
      $error = 'Cross-Site-Request-Forgery';
    }
}

require "../src/create_token.php";

?>
<?php
if (isset($error)) {
  echo "<div class='card-panel red darken-1'><span class='white-text'>$error</span></div>";
}
?>
<form action="<?= $_SERVER['PHP_SELF']; ?>" method="post">
  <?= $_SESSION['user_key'] ?>
  <input type="hidden" name="token" value="<?= $token ?>" />  <?php
    foreach ($stored as $method) {
      if ($userMethod['two_way_methods_id'] == $method['two_way_method_id']) {
        echo '<p><input class="with-gap" name="group1" checked type="radio" id="' . htmlentities($method['two_way_method_id']) . '_two_factor" value="' . htmlentities($method['two_way_method_id']) . '" /><label for="' . htmlentities($method['two_way_method_id']) . '_two_factor">' . htmlentities($method['method']) . '</label></p><p>' . htmlentities($method['description']) . '</p>';
      }
      else {
        echo '<p><input class="with-gap" name="group1" type="radio" id="' . htmlentities($method['two_way_method_id']) . '_two_factor" value="' . htmlentities($method['two_way_method_id']) . '" /><label for="' . htmlentities($method['two_way_method_id']) . '_two_factor">' . htmlentities($method['method']) . '</label></p><p>' . htmlentities($method['description']) . '</p>';
      }
    }
  ?>
  <button type="submit" name="two_factor_submit" id="two_factor_submit" value="update_two_factor" class="btn-large waves-effect waves-light orange">Submit Changes</button>
</form>
