<?php
require_once '../src/authenticate.php';
require_once '../src/db_connect.php';
include '../components/profile_pic.php';
    $allMessages = $db->prepare('SELECT users.user_alias, wall_chat.w_id, wall_chat.users_user_key, wall_chat.message, wall_chat.creation_date FROM wall_chat JOIN users ON users.user_key = wall_chat.users_user_key  order by wall_chat.creation_date desc');
    $allMessages->execute();
    $stored = $allMessages->fetchAll();

    $text;
    $text = "<div class='messageCollection'>";
    foreach ($stored as $message) {
      if($message['users_user_key'] == $_SESSION['user_key']){
        $text .= "<div class='card'>
            <div class='card-content'>
            <div class='row valign-wrapper' style='margin-bottom:0px;'>
                        <div class='col m1 s1'>
                          <div alt='your-avatar' class='circle responsive-img' style='background-image: url(".$profile_pic.");width: 4.2vw;height: 4.2vw;background-size: cover; background-position:center;'></div>
                        </div>
                        <div class='col m11 s10'>
                          <span class='black-text'>
                          <p><b>You wrote:</b><span class='right'><a data-id='".htmlentities($message['w_id'])."' class='deletePostBtn' style='cursor:pointer; margin-left:10px;'><i class='material-icons black-text'>clear</i></a></span><span class='right'>".$message['creation_date']."</span></p>
                          <p class='messageText'>".htmlentities($message['message'])."</p>
                          </span>
                        </div>
                      </div>
                      </div>
        </div>";
      }
    }

    $text .= "</div>";

?>



<div class="slider" style="position:relative;">
  <div class="circle z-depth-1 profile-img" style="background-image: url('<?php echo $profile_pic ?>');"></div>
   <ul class="slides z-depth-2">
     <li>
       <img src="http://lorempixel.com/580/250/nature/1" style="-webkit-filter: blur(5px) grayscale(0.7); filter: blur(5px) grayscale(0.7);"> <!-- random image -->
       <div class="caption center-align">
         <h3><?= htmlentities($_SESSION['user_alias']); ?></h3>
         <h5 class="light grey-text text-lighten-3">Here's our small slogan.</h5>
       </div>
     </li>
   </ul>
 </div>

 <div class="row">
   <h5>Your posts</h5>
   <?php echo $text; ?>
 </div>
