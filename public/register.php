<?php

$errors = [];
if (isset($_POST['register'])) {
  require_once '../src/db_connect.php';
  $expected = ['username', 'pwd', 'user_alias', 'confirm'];
  // Assign $_POST variables to simple variables and check all fields have values
  foreach ($_POST as $key => $value) {
    if (in_array($key, $expected)) {
      $$key = trim($value);
      if (empty($$key)) {
        $errors[$key] = 'This field requires a value.';
      }
    }
  }
  // Proceed only if there are no errors
  if (!$errors) {
    
// Remove all illegal characters from email
    $username =  filter_var($username, FILTER_SANITIZE_EMAIL);
    if ($pwd != $confirm) {
      $errors['nomatch'] = 'Passwords do not match.';
    } else {
      // Check that the username hasn't already been registered
      $sql = 'SELECT COUNT(*) FROM users WHERE username = :username AND activated = 1';
      $stmt = $db->prepare($sql);
      $stmt->bindParam(':username', $username);
      $stmt->execute();
      if ($stmt->fetchColumn() != 0) {
        $errors['failed'] = "$username is already registered.";
      }else if(!filter_var($username, FILTER_VALIDATE_EMAIL)){
        $error['failed'] = "$username is not an email";
      }else if(strlen($pwd) < 5 ){
        $error['nomatch'] = 'Password must be longer than 5 chars';
      } else {
        try {
          // Generate a random 8-character user key and insert values into the database
          $user_key = hash('crc32', microtime(true) . mt_rand() . $username);

          //Activation mail
          $to = $username;
          $subject = 'Active your I-VIP account!';
          $headers = "From: activate@i-vip.online\r\n";
          $headers .= "MIME-Version: 1.0\r\n";
          $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

          $message = '<html><body>';
          $message .= '<h4>This is your activation key:</h4>';
          $message .= '<h1>' . $user_key . '</h1>';
          $message .= '<h4>Go to <a href="http://i-vip.online/activate.php">http://i-vip.online/activate.php</a> to activate your account with this key.</h4>';
          $message .= '</body></html>';

          mail($to, $subject, $message, $headers);

          $sql = 'INSERT INTO users (user_key, username, user_alias, pwd)
          VALUES (:key, :username, :user_alias, :pwd)';
          $stmt = $db->prepare($sql);
          $stmt->bindParam(':key', $user_key);
          $stmt->bindParam(':username', $username);
          // Store an encrypted version of the password
          $stmt->bindValue(':pwd', password_hash($pwd, PASSWORD_DEFAULT));
          $stmt->bindValue(':user_alias', $user_alias);
          $stmt->execute();

          $sqlRoles = 'INSERT INTO user_roles (roles_role_id, users_user_key)
          VALUES(2, :key)';
          $stmtRoles = $db->prepare($sqlRoles);
          $stmtRoles->bindParam(':key', $user_key);
          $stmtRoles->execute();


        } catch (\PDOException $e) {
          if (0 === strpos($e->getCode(), '23')) {
            // If the user key is a duplicate, regenerate, and execute INSERT statement again
            $user_key = hash('crc32', microtime(true) . mt_rand() . $username);
            if (!$stmt->execute()) {
              throw $e;
            }
          }
        }
        // The rowCount() method returns 1 if the record is inserted,
        // so redirect the user to the activate page
        if ($stmt->rowCount()) {
          header('Location: activate.php');
          exit;
        }
      }
    }
  }
}
?>
<!doctype html>
<html>
<head>
  <meta charset="utf-8">
  <?php include "../components/main_head.php" ?>
  <title>Create Account</title>
</head>

<body id="create">
  <main>
    <div class="section no-pad-bot" id="index-banner">
      <div class="container">
        <br><br>
        <h1 class="header center orange-text">Create Account</h1>
        <div><h5 class="header center col s12 light">An email will be sent for activation</h5></div>
        <br>
        <div class="row center">
          <div class="col l3 hide-on-small-only">&nbsp;</div>
          <div class="col l6 s12">

            <form action="<?= $_SERVER['PHP_SELF']; ?>" method="post">

              <div class="row">
                <div class="input-field col l8 offset-l2 s12">
                  <input type="text" name="username" id="username">
                  <label for="username">Email:</label>
                  <?php
                  if (isset($errors['username'])) {
                    echo htmlentities($errors['username']);
                  } elseif (isset($errors['failed'])) {
                    echo htmlentities($errors['failed']);
                  }
                  ?>
                </div>
              </div>

              <div class="row">
                <div class="input-field col l8 offset-l2 s12">
                  <input type="text" name="user_alias" id="user_alias">
                  <label for="user_alias">user name:</label>
                  <?php
                  if (isset($errors['user_alias'])) {
                    echo htmlentities($errors['user_alias']);
                  } elseif (isset($errors['failed'])) {
                    echo htmlentities($errors['failed']);
                  }
                  ?>
                </div>
              </div>

             

              <div class="row">
                <div class="input-field col l8 offset-l2 s12">
                  <input type="password" name="pwd" id="pwd">
                  <label for="pwd">Password:</label>
                  <?php
                  if (isset($errors['pwd'])) {
                    echo htmlentities($errors['pwd']);
                  }
                  ?>
                </div>
              </div>

              <div class="row">
                <div class="input-field col l8 offset-l2 s12">
                  <input type="password" name="confirm" id="confirm">
                  <label for="confirm">Confirm Password:</label>
                  <?php
                  if (isset($errors['confirm'])) {
                    echo htmlentities($errors['confirm']);
                  } elseif (isset($errors['nomatch'])) {
                    echo htmlentities($errors['nomatch']);
                  }
                  ?>
                </div>
              </div>

              <div class="row">
                <div class="input-field col s12 center-align">
                  <button type="submit" name="register" id="register" value="Create Account" class="btn-large waves-effect waves-light orange">Register</button>
                </div>
              </div>

            </form>

          </div>
        </div>
      </div>
    </div>
  </main>
  <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.5/js/materialize.min.js"></script>
</body>
</html>
