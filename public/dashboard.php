<?php
require_once '../src/authenticate.php';
require_once '../src/init.php';
require_once "../src/create_token.php";
?>
<!doctype html>
<html>
<head>
<?php include "../components/main_head.php" ?>
<title>Dashboard</title>
</head>

<body>
<?php include "../components/nav.php" ?>
<div class="container">
  <div class="row">
  <div class="col s12" style="margin-bottom: 20px;">
    <ul class="tabs">
      <li class="tab col s3"><a href="#account_overview" class="active">Account Overview</a></li>
      <li class="tab col s3"><a href="#user_info">User Info</a></li>
      <li class="tab col s3"><a href="#avatar">Avatar</a></li>
    </ul>
  </div>
  <div id="account_overview" class="col s12">
    <?php include "../components/account_overview.php" ?>
  </div>
  <div id="user_info" class="col s12">
    <div class="row">
     <?php include "../components/user_info_page.php" ?>
    </div>
  </div>
  <div id="avatar" class="col s12">
    <div class="col m6 s12 offset-m3 center">
      <?php include "../components/pic_upload.php" ?>
    </div>
  </div>
</div>


</div>

<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.5/js/materialize.min.js"></script>
<script>
$(document).ready(function(){
   $('ul.tabs').tabs();
   $('.slider').slider({full_width: true});
   $('.tooltipped').tooltip({delay: 50});

   // the "href" attribute of .modal-trigger must specify the modal ID that wants to be triggered
   $('.modal-trigger').leanModal();





});
</script>
</body>
</html>
