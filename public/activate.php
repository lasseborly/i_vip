<?php
$errors = [];
if (isset($_POST['activate'])) {
  require_once '../src/db_connect.php';
  $expected = ['userkey'];
  // Assign $_POST variables to simple variables and check all fields have values
  foreach ($_POST as $key => $value) {
    if (in_array($key, $expected)) {
      $$key = trim($value);
      if (empty($$key)) {
        $errors[$key] = 'This field requires a value.';
      }
    }
  }
  // Proceed only if there are no errors
  if (!$errors) {

    $act = 1;

    try {
      $sql = "UPDATE users SET activated = :act
              WHERE user_key = :userkey";
      $stmt = $db->prepare($sql);
      $stmt->bindParam(':act', $act);
      $stmt->bindParam(':userkey', htmlentities($userkey));
      $stmt->execute();

      header('Location: index.php');
      exit;

    } catch (Exception $e) {
        $errors['failed'] = "Activation failed";
    }

  }
}
?>
<!doctype html>
<html>
<head>
  <meta charset="utf-8">
  <?php include "../components/main_head.php" ?>
  <title>Activate Account</title>
</head>

<body id="activate">
  <main>
    <div class="section no-pad-bot" id="index-banner">
      <div class="container">
        <br><br>
        <h1 class="header center orange-text">Activate Account</h1>
        <div class="header center col s12 light"><h5 >Input the activation key</h5>
        <p>A key has been send to the email you used as username, please look in your spam filter as well</p></div>
        <br>
        <div class="row center">
          <div class="col l3 hide-on-small-only">&nbsp;</div>
          <div class="col l6 s12">

          <form action="<?= $_SERVER['PHP_SELF']; ?>" method="post">
          <div class="row">
            <div class="input-field col l8 offset-l2 s12">
              <input type="text" name="userkey" id="userkey">
              <label for="userkey">Activation key:</label>
              <?php
              if (isset($errors['userkey'])) {
                echo htmlentities($errors['userkey']);
              } elseif (isset($errors['failed'])) {
                echo htmlentities($errors['failed']);
              }
              ?>
              </div>
              </div>

              <div class="row">
                <div class="input-field col s12 center-align">
                  <button type="submit" name="activate" id="activate" value="Activate Account" class="btn-large waves-effect waves-light orange">Activate</button>
                </div>
              </div>

          </form>

          </div>
        </div>
      </div>
    </div>
  </main>

  <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.5/js/materialize.min.js"></script>
</body>
</html>
