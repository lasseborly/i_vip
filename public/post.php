<?php
require '../src/init.php';

if (!empty($_POST['messageToken'])) {
    if (hash_equals($_POST['messageToken'], $_SESSION['token'])) {

      $userMessage = htmlentities($_POST["text"]);
      $userKey = htmlentities($_POST["userKey"]);
      require_once '../src/db_connect.php';


      $sql = 'INSERT INTO wall_chat ( users_user_key, message)
               VALUES (:key , :message)';
               $stmt = $db->prepare($sql);
               $stmt->bindParam(':key', $userKey);
               $stmt->bindValue(':message', $userMessage);

               $stmt->execute();

    } else {

    }
}

?>
