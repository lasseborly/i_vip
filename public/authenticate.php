<?php

require_once '../src/init.php';

$errors = [];
if (isset($_POST['authenticate'])) {
  require_once '../src/db_connect.php';
  $expected = ['login_key'];

  foreach ($_POST as $key => $value) {
    if (in_array($key, $expected)) {
      $$key = trim($value);
      if (empty($$key)) {
        $errors[$key] = 'This field requires a value.';
      }
    }
  }

  $login_key = htmlentities(trim($_POST['login_key']));

  if (!$errors) {

    try {

      $sql = 'SELECT login_key_expiry FROM users WHERE users.login_key = :login_key';
      $stmt = $db->prepare($sql);
      $stmt->bindParam(':login_key', $login_key);
      $stmt->execute();
      $stored = $stmt->fetch();

      $expiry = (int) $stored['login_key_expiry'];

      if ($expiry > time()) {
        $_SESSION['authenticated'] = 1;
        header('Location: wall.php');
        exit;
      }
      else {
        $errors['failed'] = "Authentication failed";
      }

    } catch (Exception $e) {
      $errors['failed'] = "Authentication failed";
    }

  }
}
?>
<!doctype html>
<html>
<head>
  <meta charset="utf-8">
  <?php include "../components/main_head.php" ?>
  <title>Authenticate Login</title>
</head>

<body id="authenticate">
  <main>
    <div class="section no-pad-bot" id="index-banner">
      <div class="container">
        <br><br>
        <h1 class="header center orange-text">Authenticate Login</h1>
        <div class="header center col s12 light"><h5 >Input the login key</h5>
          <p >A key has been send to the email you used as username, please look in your spam filter as well</p></div>
          <br>
          <div class="row center">
            <div class="col l3 hide-on-small-only">&nbsp;</div>
            <div class="col l6 s12">

              <form action="<?= $_SERVER['PHP_SELF']; ?>" method="post">
                <div class="row">
                  <div class="input-field col l8 offset-l2 s12">
                    <input type="text" name="login_key" id="login_key">
                    <label for="login_key">Login key:</label>
                    <?php
                    if (isset($errors['login_key'])) {
                      echo htmlentities($errors['login_key']);
                    } elseif (isset($errors['failed'])) {
                      echo htmlentities($errors['failed']);
                    }
                    ?>
                  </div>
                </div>

                <div class="row">
                  <div class="input-field col s12 center-align">
                    <button type="submit" name="authenticate" id="authenticate" value="Authenticate" class="btn-large waves-effect waves-light orange">Authenticate</button>
                  </div>
                </div>

              </form>

            </div>
          </div>
        </div>
      </div>
    </main>

    <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.5/js/materialize.min.js"></script>
  </body>
  </html>
