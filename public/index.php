<?php
require '../src/init.php';

include "../src/send_to_dashboard.php";

if (!empty($_POST['token'])) {
    if (hash_equals($_POST['token'], $_SESSION['token'])) {
      if (isset($_POST['login'])) {
        $username = htmlentities(trim($_POST['username']));
        $pwd = htmlentities(trim($_POST['pwd']));
        $stmt = $db->prepare('SELECT users.two_way_methods_id, users.pwd, users.user_alias, users.activated, users.user_key, roles.role_name FROM users JOIN user_roles ON users.user_key = user_roles.users_user_key JOIN roles ON roles.role_id = user_roles.roles_role_id WHERE users.username = :username');
        $stmt->bindParam(':username', $username);
        $stmt->execute();
        $stored = $stmt->fetch();

        if (password_verify($pwd, $stored['pwd']) == 1 && $stored['activated'] == 1) {
          session_regenerate_id(true);
          $_SESSION['username'] = $username;
          $_SESSION['user_key'] = $stored['user_key'];
          $_SESSION['role_name'] = $stored['role_name'];
          $_SESSION['user_alias'] = $stored['user_alias'];

          $login_key = hash('crc32', microtime(true) . mt_rand() . $username);
          //Ten minutes expiry time (set in config?)
          $login_key_expiry = time() + 600;

          $stmtLoginKey = $db->prepare('UPDATE users SET users.login_key=:login_key, users.login_key_expiry=:login_key_expiry WHERE users.user_key = :user_key');
          $stmtLoginKey->bindParam(':login_key', $login_key);
          $stmtLoginKey->bindParam(':login_key_expiry', $login_key_expiry);
          $stmtLoginKey->bindParam(':user_key', $stored['user_key']);
          $stmtLoginKey->execute();

            //Authentication mail
            $to = $username;
            $subject = 'Login to your I-VIP account!';
            $headers = "From: login@i-vip.online\r\n";
            $headers .= "MIME-Version: 1.0\r\n";
            $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

            $message = '<html><body>';
            $message .= '<h4>This is your login key:</h4>';
            $message .= '<h1>' . $login_key . '</h1>';
            $message .= '<h4>Go to <a href="http://i-vip.online/authenticate.php">http://i-vip.online/authenticate.php</a> to login to your account with this key.</h4>';
            $message .= '<h4>The key will last for 10 minutes.</h4>';
            $message .= '</body></html>';

            mail($to, $subject, $message, $headers);

            header('Location: authenticate.php');
            exit;

        } else {
          $error = 'Login failed. Check username and password.';
        }
      }
    } else {
         $error = 'Cross-site-request-forgery';
    }
}

include "../src/create_token.php";

?>
<!doctype html>
<html>
<head>
  <?php include "../components/main_head.php" ?>
  <title>I-VIP Welcomes You!</title>
</head>

<body>
  <main>
    <div class="section no-pad-bot" id="index-banner">
      <div class="container">
        <br><br>
        <h1 class="header center orange-text">I-VIP</h1>
        <div><h5 class="header center col s12 light">An exclusive way of sharing</h5></div>
        <br>
        <div class="row center">
          <div class="col l3 hide-on-small-only">&nbsp;</div>
          <div class="col l6 s12">

            <?php
            if (isset($error)) {
              echo "<div class='card-panel red darken-1'><span class='white-text'>" . htmlentities($error). "</span></div>";
            }
            ?>

            <form action="<?= $_SERVER['PHP_SELF']; ?>" method="post">
              <input type="hidden" name="token" value="<?php echo htmlentities($token); ?>" />
              <div class="row">
                <div class="input-field col l8 offset-l2 s12">
                  <input type="text" name="username" id="username">
                  <label for="username">Email:</label>
                </div>
              </div>
              <div class="row">
                <div class="input-field col l8 offset-l2 s12">
                  <input type="password" name="pwd" id="pwd">
                  <label for="pwd">Password:</label>
                </div>
              </div>
              <div class="row">
                <div class="input-field col s12 center-align">
                  <button type="submit" name="login" id="login" value="Log In" class="btn-large waves-effect waves-light orange">Log In</button>
                </div>
              </div>
              <div class="row">
                <a href="register.php">register</a>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </main>

  <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.5/js/materialize.min.js"></script>
</body>
</html>
