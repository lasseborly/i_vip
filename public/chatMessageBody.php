<?php
require_once '../src/authenticate.php';
require_once '../src/db_connect.php';
include '../components/profile_pic.php';
    $allMessages = $db->prepare('SELECT users.user_alias, wall_chat.w_id, wall_chat.users_user_key, wall_chat.message, wall_chat.creation_date FROM wall_chat JOIN users ON users.user_key = wall_chat.users_user_key  order by wall_chat.creation_date desc');
    $allMessages->execute();
    $stored = $allMessages->fetchAll();

    $text;
    $text = "<div class='messageCollection'>";
    foreach ($stored as $message) {
      $messageAvatar = "/uploads/profile_pics/".$message['users_user_key'].".png";

      if($message['users_user_key'] == $_SESSION['user_key']){
        $messageSender = "You";
        $messageDelete = "<span class='right'><a data-id='".$message['w_id']."' class='deletePostBtn' style='cursor:pointer; margin-left:10px;'><i class='material-icons black-text'>clear</i></a></span>";
      }else{
        $messageSender = $message["user_alias"];
        if($_SESSION['role_name'] == 'Admin'){
          $messageDelete = "<span class='right'><a data-id='".$message['w_id']."' class='deletePostBtn' style='cursor:pointer; margin-left:10px;'><i class='material-icons black-text'>clear</i></a></span>";
        }else{
          $messageDelete = "";
        }
      };


        $text .= "<div class='card '>
            <div class='card-content'>

            <div class='row valign-wrapper' style='margin-bottom:0px;'>
                        <div class='col m1 s1'>
                          <div alt='your-avatar' class='circle responsive-img' style='background-image: url(".$messageAvatar.");width: 4.2vw;height: 4.2vw;background-size: cover; background-position:center;'></div>
                        </div>
                        <div class='col m11 s10'>
                          <span class='black-text'>
                          <p><b>".$messageSender." wrote:</b>".$messageDelete."<span class='right'>".$message['creation_date']."</span></p>
                          <p class='messageText'>".$message['message']."</p>
                          </span>
                        </div>
                      </div>
                      </div>
        </div>";

      }

    $text .= "</div>";



echo $text;

?>

<script type="text/javascript">


</script>
