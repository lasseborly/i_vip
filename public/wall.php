<?php
require_once '../src/authenticate.php';
?>
<!doctype html>
<html>
<head>
<?php include "../components/main_head.php" ?>
<title>Wall</title>
<link rel="stylesheet" type="text/css" href="css/style.css">
</head>

<body>
<?php include "../components/nav.php" ?>
<div class="container">
  <div class="row">
  <div class="col s12" style="margin-bottom: 20px;">
    <ul class="tabs">
      <li class="tab col s3"><a href="#post_page" class="active">Live Chat wall</a></li>
      <?php if ($_SESSION['role_name'] == 'Admin'): ?>
      <li class="tab col s3"><a href="#users_page">Users</a></li>
      <?php endif; ?>
    </ul>
  </div>
  <div id="post_page" class="col s12">
    <?php include "../components/mind.php" ?>
  </div>
  <?php if ($_SESSION['role_name'] == 'Admin'): ?>
    <div id="users_page" class="col s12">
      <div class="row">
       <?php include "../components/users.php" ?>
      </div>
    </div>
  <?php endif; ?>
</div>
</div>

<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.5/js/materialize.min.js"></script>
<script type="text/javascript">

// if user submits chat form on mind.php
$(document).ready(function(){
  loadLog();


  $("#submitmsg").click(function(){
    var clientmsg = $("#textarea1").val();
    var inputKey = $("#user_id").val();
    var token_id = $("#token_id").val();

    $.post("post.php", {text: clientmsg, userKey: inputKey, messageToken: token_id}, function(data, status){

      loadLog();
    });

    $("#textarea1").val("");

    return false;
  });


  //Load the file containing the chat log
  function loadLog(){

    var oldscrollHeight = $("#chatbox").attr("scrollHeight") - 20; //Scroll height before the request
    $.ajax({
      url: "chatMessageBody.php",
      success: function(data){
        $("#chatbox").html(data); //Insert chat log into the #chatbox div

        //Auto-scroll
        var newscrollHeight = $("#chatbox").attr("scrollHeight") - 20; //Scroll height after the request
        if(newscrollHeight > oldscrollHeight){
          $("#chatbox").animate({ scrollTop: newscrollHeight }, 'normal'); //Autoscroll to bottom of div
        }


          $(".deletePostBtn").click(function(){
            var messageId = $(this).attr("data-id");
            $.post("deletePost.php", {message_id: messageId}, function(data, status){

              setTimeout(loadLog,1);
            });

          });

          $('.commentPostBtn').click(function(){
              alert("YOU CLIKCED ME ");
          });



        },
    });
  }

 setInterval (loadLog, 500);

});



$(document).ready(function(){

   $('ul.tabs').tabs();
   $('.slider').slider({full_width: true});
 });


$(document).ready(function(){
  // the "href" attribute of .modal-trigger must specify the modal ID that wants to be triggered
  $('.modal-trigger').leanModal();
});
</script>
</body>
</html>
