<?php
if (isset($_SESSION['authenticated']) || isset($_SESSION['ivip_auth'])) {
  header('Location: wall.php');
  exit;
}
