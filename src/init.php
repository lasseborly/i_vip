<?php
use i_vip\src\classes\PersistentSessionHandler;

require_once 'db_connect.php';
require_once 'classes/PersistentSessionHandler.php';

$handler = new PersistentSessionHandler($db);
session_set_save_handler($handler);
session_start();
$_SESSION['active'] = time();
